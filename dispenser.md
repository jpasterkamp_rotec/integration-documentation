# Dispenser

Keep in mind that each physical dispenser is treated as a logical entity with its own command queue.

This means that the central payment system(s) should receive messages for each of the dispenser-queues.

## Commands

- `activate` - Activate the given dispenser. When receiving this command, a payment and refueling validation should be initiated, for example by checking account balance, or by asking the customer for confirmation using a mobile app. The fields of the message are individual for each energy company, based on the specific information needed to identify the customer during the payment and refueling.

  - Message content
    ```json
    {
      "customerCardId": "123214567",
      "cardType": "private"
    }
    ```
    _Example fields_

- `deactivate` - Deactivate the dispenser for some reason. For example, if the dispenser is activated but the robot fails to open the fuel inlet of the car, and therefore never grabs the nozzle, the dispenser needs to be deactivated by this command.

## Events

All events should be published to the topic named `dispenser-event`, for example:

```json
TopicArn: "arn:aws:sns:eu-west-1:719432241830:dispenser-event"
```

The `Message` content must be a JSON-object containing all the data that is found relevant for each case - in addition to any required fields listed for a specific event.

### Status

#### General for dispenser

- `idle` - the dispenser is idle and ready for activation.

#### Session specific

- `awaiting_approval` - initial status when the `activate` command has been received.
- `activated` - when the dispenser has been activated and is ready for refueling.
- `limit_reached` - when a limited amount of fuel is set, and that limit has been reached.
- `stopped` - when the refueling has been stopped, for example by a mobile app.
- `finished` - when the refueling is finished i.e. the nozzle is - `finished` - when the refueling is finished i.e. the nozzle is back in place.

### Error

#### General for dispenser

- `internal_error` - an internal error has occured.

#### Session specific

- `payment_failure` - error validating the payment of the account provided in the activation command.
- `system_failure` - other system errors not related to the specific customer.
- `dispenser_not_found` - Attempt to do operation on non existing dispenser.
- `dispenser_busy` - attempt to create new payment session on a non-idle dispenser.
- `session_already_initialized` - attempt to start payment session which already exists.
- `session_not_found` - attempt to do operation on non existing payment session.
- `session_has_ended` - attempt to execute command on already ended payment session.

The message content must be a JSON-object containing all the data that is found relevant for each case.

## Usage

When the Autofuel refuel manager gets an event message from the robot, telling that the car is parked, it should request the dispenser to activate, by sending an `activate` command to the specific dispenser's queue.

The energy company will then approve the payment details tied to the vehicle's account and send a message on the `dispenser-event` topic, so the Autofuel refuel manager knows that the dispenser is activated and the robotic refueling process can be started.

When the nozzle is put back in place, the energy company should send a `status` message with the value `finished` to the `dispenser-event` topic, and if the customer has chosen a limited amout of fuel, it should send a `status` message with the value `limit_reached` to the `dispenser-event` topic when that amount has been reached.

## Examples

### Requesting dispenser activation

Received in queue: `dispenser--ROBOT_1_LEFT_DISPENSER`

```json
{
  "MessageAttributes": {
    "messageType": "command",
    "entityType": "dispenser",
    "entityId": "ROBOT_1_LEFT_DISPENSER",
    "value": "activate",
    "senderId": "arn:aws:lambda:eu-west-1:719432241830:function:requestDispenser",
    "transactionId": "ab347520-92a0-4807-af25-432002d3c329"
  },
  "Message": {
    "customerId": "98765434"
  }
}
```

### Dispenser has been activated

Sent to topic: `dispenser-event`

```json
{
  "MessageAttributes": {
    "messageType": "event",
    "entityType": "dispenser",
    "entityId": "ROBOT_1_LEFT_DISPENSER",
    "value": "activated",
    "valueType": "status",
    "senderId": "dispenser--ROBOT_1_LEFT_DISPENSER",
    "transactionId": "ab347520-92a0-4807-af25-432002d3c329"
  },
  "Message": {}
}
```

### Reach limit

Sent to topic: `dispenser-event`

```json
{
  "MessageAttributes": {
    "messageType": "event",
    "entityType": "dispenser",
    "entityId": "ROBOT_1_LEFT_DISPENSER",
    "value": "limit_reached",
    "valueType": "status",
    "senderId": "dispenser--ROBOT_1_LEFT_DISPENSER",
    "transactionId": "ab347520-92a0-4807-af25-432002d3c329"
  },
  "Message": {}
}
```

### Payment failure

Sent to topic: `dispenser-event`

```json
{
  "MessageAttributes": {
    "messageType": "event",
    "entityType": "dispenser",
    "entityId": "ROBOT_1_LEFT_DISPENSER",
    "value": "payment_failure",
    "valueType": "error",
    "senderId": "dispenser--ROBOT_1_LEFT_DISPENSER",
    "transactionId": "ab347520-92a0-4807-af25-432002d3c329"
  },
  "Message": {
    "any relevant data": "should be here"
  }
}
```
