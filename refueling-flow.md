# Refueling flow

![refueling flow with app](images/refueling_process_using_RFID_and_app.svg)

![refueling flow without app](images/refueling_process_using_RFID_only.svg)
